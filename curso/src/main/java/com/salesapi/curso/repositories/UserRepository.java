package com.salesapi.curso.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salesapi.curso.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
