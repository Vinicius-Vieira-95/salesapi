package com.salesapi.curso.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salesapi.curso.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
    
}
