package com.salesapi.curso.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salesapi.curso.entities.Product;

public interface ProductRepository  extends JpaRepository<Product, Long> {
    
}
