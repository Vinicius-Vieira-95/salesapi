package com.salesapi.curso.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.salesapi.curso.entities.Product;
import com.salesapi.curso.repositories.ProductRepository;

@Service
public class ProductService {
    
    @Autowired
    private ProductRepository repository;


    public List<Product> findAll() {
        return repository.findAll();
    }
    
    public Product findById( Long id) {
        Product prod = repository.getReferenceById(id);
        Product newProd = new Product(prod.getId(), prod.getName(),
            prod.getDescription(), prod.getPrice(), prod.getImgUrl());
        return newProd;
    }
}
