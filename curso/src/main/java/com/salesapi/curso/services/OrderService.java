package com.salesapi.curso.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.salesapi.curso.entities.Order;
import com.salesapi.curso.repositories.OrderRepository;

@Service
public class OrderService {
  
    @Autowired
    private OrderRepository repository;

    public List<Order> findAll() {
        return repository.findAll();
    }
    
    public Order findById( Long id) {
        Order order = repository.getReferenceById(id);
        Order newOrder = new Order(order.getId(), order.getDate(), order.getClient(), order.getStatus(), order.getItems(), order.getPayment());
        return newOrder;
    }

}
